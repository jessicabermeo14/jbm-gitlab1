import pandas as pd
import matplotlib.pyplot as plt
import math
import numpy as np

 
def identificarMes(df):
    
    df["FECHA"] = df["FECHA"].str.split('/').str.get(1)
        
    df["FECHA"] = df["FECHA"].str.replace('01','ENERO')
    df["FECHA"] = df["FECHA"].str.replace('02','FEBRERO')
    df["FECHA"] = df["FECHA"].str.replace('03','MARZO')
    df["FECHA"] = df["FECHA"].str.replace('04','ABRIL')
    df["FECHA"] = df["FECHA"].str.replace('05','MAYO')
    df["FECHA"] = df["FECHA"].str.replace('06','JUNIO')
    df["FECHA"] = df["FECHA"].str.replace('07','JULIO')
    df["FECHA"] = df["FECHA"].str.replace('08','AGOSTO')
    df["FECHA"] = df["FECHA"].str.replace('09','SEPTIEMBRE')
    df["FECHA"] = df["FECHA"].str.replace('10','OCTUBRE')
    df["FECHA"] = df["FECHA"].str.replace('11','NOVIEMBRE')
    df["FECHA"] = df["FECHA"].str.replace('12','DICIEMBRE')

    return df

def graficarBarras(columna, ejex, ejey):
    datosX = pd.unique(columna)
    datosY = contenidoPorColumna(columna)
    titulo = ejey + " VS " + ejex
    print(titulo)
    y_pos = np.arange(len(datosX))
    plt.bar(y_pos,datosY)
    plt.xticks(y_pos,datosX) 
    plt.title(titulo)
    plt.xlabel(ejex)
    plt.ylabel(ejey)
    plt.grid()
    plt.show()


def graficarBarras2(t,columnaX, columnaY, ejex, ejey, dt, nombreColumnaY):
    datosX = pd.unique(columnaX)
    datosX = pd.unique(columnaX)
    if t == 0:
        datosY = contenidoPorColumna(columnaX)    
    else:
        datosY = relacionDatos2(dt,columnaX,columnaY,ejey)
 
    titulo = ejey + " VS " + ejex
    print(titulo)
    y_pos = np.arange(len(datosX))
    plt.bar(y_pos,datosY)
    plt.xticks(y_pos,datosX) 
    plt.title(titulo)
    plt.xlabel(ejex)
    plt.ylabel(ejey)
    plt.grid()
    plt.show()

# Devuelve un vector con la cantidad de veces que se repite un dato en una columna.
def contenidoPorColumna(columna):
    contenidoColumna = pd.unique(columna)
    cantidadColumna = np.zeros(len(contenidoColumna))
    for i in range(len(contenidoColumna)):
        cantidadColumna[i] = (columna == contenidoColumna[i]).sum()
    return cantidadColumna


# Devuelve un valor de la relacion de un dato con otro de una columna diferente.
# Datos cruzados.

def relacionDatos(dt,nombreColumnaX,nombreColumnaY,datoX,datoY):
    cantidad = 0
    for i in range(0,len(dt)):
        if ((df.loc[i,nombreColumnaX] == datoX) and (df.loc[i,nombreColumnaY] == datoY)):
            cantidad = cantidad + 1
    return cantidad


# Devuelve un vector con la cantidad de veces que se repite un dato en una columna.
def relacionDatos2(dt,columnaX,columnaY,nombreColumnaY):
    contenidoColumnaX = pd.unique(columnaX)
    ventorRelacionColumnas = np.zeros(len(contenidoColumnaX))
    for i in range(len(contenidoColumnaX)):
        ventorRelacionColumnas[i] = relacionDatos(dt, contenidoColumnaX[i], nombreColumnaY,
                                                      contenidoColumnaX[i], nombreColumnaY)
                    
    return ventorRelacionColumnas 

# Devuelve un vector con la cantidad de veces que se repite un dato en una columna.
def relacionDatos3(dt,columnaX,columnaY,nombreColumnaX,nombreColumnaY):
    contenidoColumnaX = pd.unique(columnaX)
    contenidoColumnaY = pd.unique(columnaY)
    ventorRelacionColumnas = np.zeros(len(contenidoColumnaX))
    ventorTodasLasRelaciones = []
    for j in range(len(contenidoColumnaY)):
        for i in range(len(contenidoColumnaX)):
            ventorRelacionColumnas[i] = relacionDatos(dt, nombreColumnaX, nombreColumnaY,
                                                      contenidoColumnaX[i], contenidoColumnaY[j])
        ventorTodasLasRelaciones.append(ventorRelacionColumnas)
                    
    return  ventorTodasLasRelaciones   


        
accidentalidad2014 = pd.read_csv("Accidentalidad2014.txt", header= "infer")
df=pd.DataFrame(accidentalidad2014)
df = identificarMes(df)
contenidoPorColumna(df["DIA"])

# imprimir graficas

graficarBarras2(0,df["DIA"], df["DIA"], "DIA", "CANTIDAD DE ACCIDENTES",df, "CANTIDAD DE ACCIDENTES")
graficarBarras2(0,df["FECHA"], df["FECHA"], "MESES", "CANTIDAD DE ACCIDENTES",df, "CANTIDAD DE ACCIDENTES")
graficarBarras2(0,df["GRAVEDAD"], df["GRAVEDAD"], "TIPO DE ACCIDENTE", "CANTIDAD DE ACCIDENTES",df, "CANTIDAD DE ACCIDENTES")
graficarBarras2(0,df["COMUNA"], df["COMUNA"], "COMUNA", "CANTIDAD DE ACCIDENTES", df, "CANTIDAD DE ACCIDENTES")
graficarBarras2(1,df["DISEÑO"], df["DISEÑO"], "DISÑO DE VIA", "CANTIDAD DE ACCIDENTES", df, "CANTIDAD DE ACCIDENTES")


#graficarBarras2(t,columnaX, columnaY, ejex, ejey, dt, nombreColumnaY)



