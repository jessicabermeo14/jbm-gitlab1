'use strict'

const express = require('express')
const app = express()

app.get('/', (req,res) => {
    res.send('<h1>Hello World</h1>')
})

app.get('/status/:parameter', (req,res) => {
    console.log(req.params)
    let param = req.params.parameter
    res.send('The Status is'+ param)

})


app.listen(3000, ()=>{
});