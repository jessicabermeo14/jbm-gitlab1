from flask import Flask
from flask import request
from wtforms import Form, StringField, TextField
from wtforms.fields import EmailField

class CommentForm(Form):
    username = StringField('username')
    email = EmailField('correo electronico')
    comment = TextField('comentario')


app = Flask(__name__) # Nuevo objeto.

@app.route('/')# Decorador.
def hello_world():
    return 'Hello world!'

#http://localhost:8000/params
#http://localhost:8000/params?params1=nombre
@app.route('/params')
def params():
    param = request.args.get('params1','no contiene este parametro')
    return 'otro msj!'

@app.route('/saluda/')
@app.route('/saluda<name>')
@app.route('/saluda<name>/<last_name>')
def params2(name = 'Este es nombre por default',last_name = 'nada'):
    return ' Hola : {} {}'.format(name,last_name)



if __name__ == '__main__':
    @app.run(debug = True, port= 8000) #Debug carga de inmediato.      
    #@app.run() # Se encarga de ejecutar por defecto en el puerto 5000.
